import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Platform,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
} from "react-native";

export default class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text
          style={{
            fontSize: 30,
            color: "white",
            textAlign: "center",
            fontFamily: "sans-serif",
          }}
        >
          LOGIN
        </Text>
        <View style={styles.containerLogin}>
          <Text style={styles.inputTitle}>Username</Text>
          <TextInput
            style={{
              height: 40,
              width: 300,
              backgroundColor: "white",
              borderRadius: 30,
              marginBottom: 10,
            }}
          />
          <Text style={styles.inputTitle}>Password</Text>
          <TextInput
            style={{
              height: 40,
              width: 300,
              backgroundColor: "white",
              borderRadius: 30,
              marginBottom: 30,
            }}
          />
          <View style={{ width: 150, borderRadius: 30, overflow: "hidden" }}>
            <Button color="black" title="Login" />
          </View>
        </View>
        <Image
          source={require("./assets/images/logo.png")}
          style={{ marginLeft: 130, marginTop: 50 }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#00B0A6",
    justifyContent: "center",
  },
  containerLogin: {
    marginHorizontal: 40,
    height: 300,
    alignItems: "center",
    justifyContent: "center",
  },
  inputTitle: {
    fontSize: 18,
    color: "white",
    marginVertical: 7,
  },
});
