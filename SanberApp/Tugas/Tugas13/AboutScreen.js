import React, { Component } from 'react'
import { View, StyleSheet, Platform, Text, Image, TouchableOpacity, FlatList, TextInput, Button  } from 'react-native'
import { Ionicons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons'; 
import { Entypo } from '@expo/vector-icons'; 

export default class AboutScreen extends Component {
    render() {
        
      return (
       
        <View style={styles.container}>
                <View style={{marginBottom: 30}}>
                    <TouchableOpacity>
                    <Entypo name="back" size={24} color="black" />
                    </TouchableOpacity>
                </View>
                <Text style={{fontSize: 30, textAlign: 'center', color: 'white'}}>Tentang Saya</Text>

                <View style={styles.content}>
                    <Image source={require('./assets/images/profile.jpg')} style={{borderRadius: 25}} />
                    <Text style={{marginTop: 10, marginBottom: 40, fontSize: 28, textAlign: 'center', color: 'white'}}>Nurrul Fakhri</Text>                  
                </View>

                    <View style={styles.sosmed}>
                        <Ionicons name="logo-twitter" size={25} color= 'white'/>
                        <Text style={{ fontSize: 20, color: 'white', marginLeft: 8}}>@nurrulfakhri</Text>
                    </View>
                    <View style={styles.sosmed}>
                        <Ionicons name="logo-instagram" size={25} color= 'white'/>
                        <Text style={{ fontSize: 20, color: 'white', marginLeft: 12}}>@nfakhri12</Text>
                    </View>
                    <View style={styles.sosmed}>
                        <Ionicons name="logo-facebook" size={25} color= 'white'/>
                        <Text style={{ fontSize: 20, color: 'white', marginLeft: 13}}>Nurrul Fakhri</Text>
                    </View>
                    <View style={styles.sosmed}>
                        <AntDesign name="gitlab" size={25} color= 'white'/>
                        <Text style={{ fontSize: 20, color: 'white', marginLeft: 8}}>N Fakhri</Text>
                    </View>
              
                       
                       
                
         
        </View>

      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#00B0A6',
      padding: 20
    },
    content: {
        marginTop: 20,
        alignItems: 'center'
    },
    sosmed: {
        marginTop: 20,
        marginLeft: 125,
        flexDirection: 'row',
    },
  
   
  


});