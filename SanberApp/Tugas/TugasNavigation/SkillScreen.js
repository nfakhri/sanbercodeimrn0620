import React, { Component } from 'react'
import { 
    View, 
    StyleSheet, 
    Platform, 
    Text, 
    Image,
    TouchableOpacity, 
    FlatList,
    Slider,  
    ScrollView,
    SafeAreaView    
} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import { Entypo } from '@expo/vector-icons'; 



export default class SkillScreen extends Component {

    state = {
        slideValueJavaScript:90,
        slideValueReactNative:80,
        slideValueGitlab:85
    }
    render() {
        
      return (
       
        <SafeAreaView style={styles.container}>
                <View style={{marginBottom: 30}}>
                    {/* <TouchableOpacity>
                    <Entypo name="back" size={24} color="black" />
                    </TouchableOpacity> */}
                </View>
                <Text style={{fontSize: 30, textAlign: 'center', color: 'white'}}>Daftar Skill Programming</Text>
             
                <View style={styles.content}>
                    <View style={{flexDirection: 'row',justifyContent: 'space-between'}}>
                        <MaterialCommunityIcons name="account" size={50} color="black" />

                        <Text style={styles.user}>Nama User</Text> 
                        
                    </View>
                    <ScrollView>
                    <View style={{height:0.5,backgroundColor:'black', marginVertical: 20}}/>
                    <View style={styles.skill}>
                        <MaterialCommunityIcons name="language-javascript" size={50} color= 'black'/>
                        <View style={styles.descriptionSkill}>
                            <Text style={styles.titleSkill}>Java Script</Text>
                            <Text style={styles.categorySkill}>Programming</Text>
                            <Slider value={this.state.slideValueJavaScript}  
                                    minimumValue={0}
                                    maximumValue={100}
                                    maximumTrackTintColor="#FF0000"
                                    minimumTrackTintColor="#00B0A6"
                                    thumbTintColor='#00B0A6'
                                    disabled
                                    />
                            <Text style={{fontWeight: 'bold'}}>Persentase skill : {this.state.slideValueJavaScript} %</Text>
                        </View>
                    </View>
                    <View style={styles.skill}>
                        <MaterialCommunityIcons name="react" size={50} color= 'black'/>
                        <View style={styles.descriptionSkill}>
                            <Text style={styles.titleSkill}>React Native</Text>
                            <Text style={styles.categorySkill}>Framework / Library</Text>
                            <Slider value={this.state.slideValueGitlab}  
                                    minimumValue={0}
                                    maximumValue={100}
                                    maximumTrackTintColor="#FF0000"
                                    minimumTrackTintColor="#00B0A6"
                                    thumbTintColor='#00B0A6'
                                    disabled
                                    />
                            <Text style={{fontWeight: 'bold'}}>Persentase skill : {this.state.slideValueReactNative} %</Text>
                        </View>
                    </View>
                    <View style={styles.skill}>
                        <MaterialCommunityIcons name="gitlab" size={50} color= 'black'/>
                        <View style={styles.descriptionSkill}>
                            <Text style={styles.titleSkill}>Gitlab</Text>
                            <Text style={styles.categorySkill}>Teknologi</Text>
                            <Slider value={this.state.slideValueGitlab}  
                                    minimumValue={0}
                                    maximumValue={100}
                                    maximumTrackTintColor="#FF0000"
                                    minimumTrackTintColor="#00B0A6"
                                    thumbTintColor='#00B0A6'
                                    disabled
                                    />
                            <Text style={{fontWeight: 'bold'}}>Persentase skill : {this.state.slideValueGitlab} %</Text>
                        </View>
                    </View>
                    </ScrollView> 
                        
                </View>           
         
        </SafeAreaView>

      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#00B0A6',
      padding: 20,
      
    },
    content: {
        marginTop: 50,
        paddingHorizontal: 20,
        paddingTop: 20,
        backgroundColor: 'white',
        borderRadius: 10
    },
    profil: {
        marginTop: 10,
        borderRadius: 30
    },
    user: {
        fontSize: 18,
        marginRight: 10,
        marginTop: 20
    },
    skill: {   
        marginVertical: 30,
        flexDirection: 'row',
    },
    descriptionSkill: {
       flex: 1,
       marginLeft: 30,
       alignItems: "stretch"

    },
    titleSkill: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    categorySkill: {
        fontSize: 14,

    }
  
   
  


});