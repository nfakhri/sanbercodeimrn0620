import React from "react";
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer'

import About from './AboutScreen';
import Add from './AddScreen';
import Login from './LoginScreen';
import Project from './ProjectScreen';
import Skill from './SkillScreen';

const RootStack = createStackNavigator();
const SkillStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const AddStack = createStackNavigator();
const AboutStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();

const SkillStackScreen = () => (
    <SkillStack.Navigator>
        <SkillStack.Screen name='Skill' component={Skill}/>
    </SkillStack.Navigator>
)
const ProjectStackScreen = () => (
    <ProjectStack.Navigator>
        <ProjectStack.Screen name='Project' component={Project}/>
    </ProjectStack.Navigator>
)
const AddStackScreen = () => (
    <AddStack.Navigator>
        <AddStack.Screen name='Add' component={Add}/>
    </AddStack.Navigator>
)
const AboutStackScreen = () => (
    <AboutStack.Navigator>
        <AboutStack.Screen name='About' component={About}/>
    </AboutStack.Navigator>
)

const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name='Skill' component={SkillStackScreen}/>
        <Tabs.Screen name='Project' component={ProjectStackScreen}/>
        <Tabs.Screen name='Add' component={AddStackScreen}/>
    </Tabs.Navigator>
)

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name='Home' component={TabsScreen} />
        <Drawer.Screen name='About' component={AboutStackScreen} />
    </Drawer.Navigator>
)

export default() => (
    <NavigationContainer>
        {/* <Root.Navigator>
            <Root.Screen name='Login' component={Login}/>
            <Root.Screen name='Drawer' component={DrawersScreen}/>
        </Root.Navigator> */}
        
        <Drawer.Navigator>
        <Drawer.Screen name='Home' component={TabsScreen} />
        <Drawer.Screen name='About' component={AboutStackScreen} />
    </Drawer.Navigator>
       
    </NavigationContainer>
)