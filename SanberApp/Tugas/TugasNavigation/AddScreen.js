import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import { StatusBar } from 'expo-status-bar';

export default function App(){
    return(
        <View style={styles.container}>
            <Text style={{color: 'white'}}>Halaman Tambah</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#00B0A6',
        justifyContent: 'center',
        alignItems: 'center'
    }
})