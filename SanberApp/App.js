import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

import YoutubeUI from "./Tugas/Tugas12/App";
import LoginScreen from "./Tugas/Tugas13/LoginScreen";
import AboutScreen from "./Tugas/Tugas13/AboutScreen";
import Tugas14 from "./Tugas/Tugas14/App";
import Tugas15 from "./Tugas/Tugas15";
import TugasNavigation from "./Tugas/TugasNavigation";
import Quiz3 from "./Tugas/Quiz3";
import Api from "./Tugas/Api/Api";

export default function App() {
  return (
    // <YoutubeUI />
    // <LoginScreen/>
    // <AboutScreen/>
    // <Tugas14/>
    // <Tugas15/>
    // <TugasNavigation/>
    // <Quiz3 />
    <Api />

    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
