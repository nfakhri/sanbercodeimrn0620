// NO 1
console.log('No 1')

function teriak(){
    return 'Halo Sanbers!'
}

console.log(teriak())
console.log('\n')

// NO 2
console.log('No 2')

function multiply(num1, num2){
    return num1 * num2
}

var num1 = 12
var num2 = 4

var hasilKali = multiply(num1, num2)
console.log(hasilKali);
console.log('\n')

// NO 3
console.log('No 3')

function introduce(name, age, address, hobby){
    return 'Nama Saya '+name+ ', umur saya '+age+ ' tahun, alamat saya di '+address+ ' dan saya punya hobby yaitu '+hobby
}

 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)