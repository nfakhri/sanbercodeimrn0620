console.log('No 1')

const golden = () => {
    console.log("this is golden!!")
}

golden()
console.log('\n')

console.log('No 2')
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName : () => {
            console.log(firstName + " " + lastName)
            
        }

    }
}

newFunction("William", "Imoh").fullName()
console.log('\n')

console.log('No 3')

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const { firstName, lastName, destination, occupation} = newObject;

console.log(firstName, lastName, destination, occupation)
console.log('\n')

console.log('No 4')

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)
console.log('\n')

console.log('No 4')
const planet = "earth"
const view = "glass"
var before = `Lorem ${planet} dolor sit amet consectetur adipiscing elit ${view} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code
console.log(before)