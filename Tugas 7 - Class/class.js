// NO 1 ANIMAL CLASS

console.log('No 1 Animal Class')

class Animal{

    constructor(name){
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }

}

var sheep = new Animal('shaun')

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)
console.log('\n')


class Frog extends Animal{

    jump(){
        console.log('hop hop')
    }
}

class Ape extends Animal{
    constructor(name){
        super(name)
        this.legs = 2
    }
    yell(){
        console.log('Auooooo')
    }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)
sungokong.yell() 
console.log('\n')
 
var kodok = new Frog("buduk")
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)
kodok.jump() 
console.log('\n')

// NO 2 FUNCTION TO CLASS

console.log('No 2 Function to Class')



class Clock {
    
    constructor({template}){
        this.template = template
      
    }
    
    render = () => {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
          
        console.log(output);
    }

    stop() {
        clearInterval(timer);
    }
    
    start(){
        this.render();
        var timer= setInterval(this.render, 1000);
    }
   
    
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
