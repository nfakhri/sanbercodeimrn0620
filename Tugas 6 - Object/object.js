// NO 1 Array to Object

console.log('No 1 Array to Object')

function arrayToObject(arr) {
    for (const i in arr){

        var now = new Date()
        var thisYear = now.getFullYear() 

        if((arr[i][3] > thisYear) || (!arr[i][3])){
            myAge = "Invalid Birth Year"
        }else{
            var myAge = thisYear - arr[i][3]
        }

        if (arr){
            var obj = {
                firstName : arr[i][0],
                lastName : arr[i][1],
                gender : arr[i][2],
                age : myAge
            }
        }else{
            return "";
        }
        
        
        var no = parseInt(i) + 1
        console.log(no+ '. '+ obj.firstName + ' '+ obj.lastName + ' :')
        console.log(obj)
        
        
    }
    
    
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

console.log('\n')


// NO 2 Shopping time

console.log('No 2 Shopping Time')

function shoppingTime(memberId, money) {
    if (!memberId){
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }else{
        if (money < 50000){
            return 'Mohon maaf uang tidak cukup'
        }else{
            var obj = {}
            var listPurchased = []

            obj.memberId = memberId
            obj.money = money
            


            if (money >= 1500000){
               listPurchased.push('Sepatu Stacattu')
               money -= 1500000 
               
            }
            if(money >= 500000){
                listPurchased.push('Baju Zoro')
                money -= 500000
                
            }
            if(money >= 250000){
                listPurchased.push('Baju H&N')
                money -= 250000 
               
            }
            if(money >= 175000){
                listPurchased.push('Sweater Uniklooh')
                money -= 175000 
                
            }
            if(money >= 50000){
                listPurchased.push('Casing  Handphone ')
                money -= 50000
                
            }
                            
            obj.listPurchased = listPurchased
            obj.changeMoney = money
           
           
           return obj
           



        }
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000));
  console.log(shoppingTime('234JdhweRxa53', 15000)); 
  console.log(shoppingTime()); 


console.log('\n')


// NO 3 Naik Angkot

console.log('No 3 Naik Angkot')

  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    if (arrPenumpang.length > 0){
        for(const i in arrPenumpang){
            var obj = {}
    
            
            var dari = rute.indexOf(arrPenumpang[i][1])
            var ke = rute.indexOf(arrPenumpang[i][2])
       
            var selisih = ke - dari
            
            
            var bayar = 2000 * selisih
           
            obj.penumpang = arrPenumpang[i][0]
            obj.naikDari = arrPenumpang[i][1]
            obj.tujuan = arrPenumpang[i][2]
            obj.bayar = bayar
    
            console.log(obj)
        }
       
    }else{
        return []
    }
    

  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  console.log(naikAngkot([])); 