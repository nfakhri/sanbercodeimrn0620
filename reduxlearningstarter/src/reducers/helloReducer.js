import { PRESSED_HELLO_BUTTON } from "../actions/types";

const initialState = {
  helloText: "Hello!",
  pressedButton: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PRESSED_HELLO_BUTTON:
      return { ...state, pressedButton: true }; // ...state artinya semua properti state diabaikan kecual properti presedButton jika ...state dihilangkan semua property hilang kecuali properti pressed button
    default:
      return state;
  }
};
