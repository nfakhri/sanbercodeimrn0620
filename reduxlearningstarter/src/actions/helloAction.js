import { PRESSED_HELLO_BUTTON } from "./types";

export const helloAction = () => {
  return {
    TYPE: PRESSED_HELLO_BUTTON,
  };
};
