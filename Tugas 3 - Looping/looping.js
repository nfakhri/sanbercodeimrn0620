// NO 1 LOOPING WHILE

console.log('No. 1 Looping While');
console.log('\n');

var i = 2;
var j = 20;

console.log('LOOPING PERTAMA');
while (i <= 20){
    console.log(i + ' - I love coding');
    i += 2; 
}

console.log('LOOPING KEDUA');
while (j >= 2){
    console.log(j + ' - I Will become a mobile developer');
    j -= 2; 
}

console.log('\n');

// NO 2 LOOPING MENGGUNAKAN FOR

console.log('No. 2 Looping menggunakan for');
console.log('\n');

for (var angka = 1; angka <= 20; angka++){
    if (((angka % 2) == 1) && ((angka % 3) == 0)){
        console.log(angka + ' - I Love Coding');
    }else if ((angka % 2) == 0){
        console.log(angka + ' - Berkualitas');
    }else if ((angka % 2) == 1){
        console.log(angka + ' - Santai');
    }
}

console.log('\n');

// NO 3 MEMBUAT PERSEGI PANJANG #

console.log('No. 3 Membuat persegi panjang');
console.log('\n');

for (a = 1; a <= 4; a++){
    for (b = 1 ; b <= 8; b++){
        process.stdout.write('#'); 
    } 
    process.stdout.write('\n'); 
    
}


console.log('\n');

// NO 4 MEMBUAT TANGGA

console.log('No. 4 Membuat Tangga');
console.log('\n');

for (c = 1; c <= 7; c++){
    for (d = 1 ; d <= c; d++){
        process.stdout.write('#'); 
    } 
    process.stdout.write('\n'); 
    
}

console.log('\n');

// NO 5 MEMBUAT PAPAN CATUR

console.log('No. 5 Membuat Papan Catur');
console.log('\n');

for (e = 1; e <= 8; e++){
    if ((e % 2) == 1 ){
        for (f = 1 ; f <= 8; f++){
            if((f % 2) == 1){
                process.stdout.write(' ');
            }else{
                process.stdout.write('#');
            }  
        } 
    }else{
        for (f = 1 ; f <= 8; f++){
            if((f % 2) == 1){
                process.stdout.write('#');
            }else{
                process.stdout.write(' ');
            }  
        } 
    }
   
    
    process.stdout.write('\n'); 
}
