// NO 1 RANGE

console.log('No 1 Range');

function range(startNum, finishNum){

    if (!startNum || !finishNum){
        return -1;
    }else{
        var number = []

        if (startNum < finishNum){
            for (var i = startNum; i <= finishNum; i++){
                number.push(i)
            }
        }else{
            for (var i = startNum; i >= finishNum; i--){
                number.push(i)
            }
        }
        
        return number
    }
}

console.log(range(1,10))
console.log('\n')

// NO 2 RANGE WITH STEP

console.log('No 2 Range With Step');

function rangeWithStep(startNum, finishNum, step){

    if (!startNum || !finishNum){
        return -1;
    }else{
        var number = []

        if (startNum < finishNum){
            for (var i = startNum; i <= finishNum; i += step){
                number.push(i)
            }
        }else{
            for (var i = startNum; i >= finishNum; i -= step){
                number.push(i)
            }
        }
        
        return number
    }
}

console.log(rangeWithStep(29, 2, 4))
console.log('\n')


// NO 3 SUM OF RANGE

console.log('No 3 Sum of Range');

function sum(startNum=0, finishNum=1, step=1){

    

    if (!startNum && !finishNum){
        return 0
    }
    else{
        var jumlah = 0;

        if (startNum < finishNum){
            for (var i = startNum; i <= finishNum; i += step){
                jumlah += i
            }
        }else{
            for (var i = startNum; i >= finishNum; i -= step){
                jumlah += i
            }
        }
        
        return jumlah
    }
}

console.log(sum(20, 10, 2))
console.log('\n')


// NO 4 ARRAY MULTIDIMENSI

console.log('No 4 Array Multidimensi')

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(){

    for(var i=0; i < input.length; i++){
        console.log('Nomor ID : '+ input[i][0])
        console.log('Nama Lengkap : '+ input[i][1])
        console.log('TTL : '+ input[i][2] +' '+ input[i][3])
        console.log('Hobi : '+ input[i][4])
        console.log('\n')
    }
}

dataHandling()

console.log('\n')


// NO 5 BALIK KATA

console.log('No 5 Balik Kata')

function balikKata(kata){
    
    var huruf = ''
    for(var i = kata.length-1; i >= 0; i--){
        
        huruf += kata[i]
    }
    
    return huruf

}

console.log(balikKata("Kasur Rusak"))

console.log('\n')


// NO 6 METODE ARRAY

console.log('No 6 Metode Array')


function dataHandling2(input){

    
    input.splice(1,2,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
    input.splice(4,1,"Pria", "SMA Internasional Metro")
    console.log(input)

    var bulan = input[3].split('/')
    switch(bulan[1]){
        case '01':{console.log('Januari');break;}
        case '02':{console.log('Februari');break;}
        case '03':{console.log('Maret');break;}
        case '04':{console.log('April');break;}
        case '05':{console.log('Mei');break;}
        case '06':{console.log('Juni');break;}
        case '07':{console.log('Juli');break;}
        case '08':{console.log('Agustus');break;}
        case '09':{console.log('September');break;}
        case '10':{console.log('Oktober');break;}
        case '11':{console.log('November');break;}
        case '12':{console.log('Desember');break;}
    }

    var x = [bulan[0], bulan[1], bulan[2]] 
    x.sort(function (value1, value2) { return value2 - value1 } ) ; 
    console.log(x)

    console.log(bulan.join('-'))

    var name = input[1].slice(0, 14)
    console.log(name)
    

    



    



}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);









