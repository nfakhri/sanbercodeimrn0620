import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import About from "./AboutScreen";
import Login from "./LoginScreen";
import Register from "./RegisterScreen";
import Home from "./redux";
import Movie from "./MovieScreen";

const RootStack = createStackNavigator();
const MovieStack = createStackNavigator();
const HomeStack = createStackNavigator();
const AboutStack = createStackNavigator();

const Tabs = createBottomTabNavigator();

const MovieStackScreen = () => (
  <MovieStack.Navigator>
    <MovieStack.Screen name="Movie" component={Movie} />
  </MovieStack.Navigator>
);

const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={Home} />
  </HomeStack.Navigator>
);

const AboutStackScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="About" component={About} />
  </AboutStack.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Home" component={HomeStackScreen} />
    <Tabs.Screen name="Movie" component={MovieStackScreen} />
    <Tabs.Screen name="About" component={AboutStackScreen} />
  </Tabs.Navigator>
);

export default () => (
  <NavigationContainer>
    <RootStack.Navigator initialRouteName="Login">
      <RootStack.Screen name="Login" component={Login} />
      <RootStack.Screen name="Register" component={Register} />
      <RootStack.Screen name="Home" component={TabsScreen} />
    </RootStack.Navigator>
  </NavigationContainer>
);
