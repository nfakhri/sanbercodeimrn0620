import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Platform,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
} from "react-native";

export default class MovieScreen extends Component {
  state = {
    title: "",
    data: [],
  };

  // componentDidMount() {
  //   this.fetchData();
  // }

  fetchData = async () => {
    const response = await fetch(
      `http://www.omdbapi.com/?apikey=dca61bcc&s=${this.state.title}`
    );
    const json = await response.json();
    this.setState({ data: json.Search });
  };

  onChangeTitle = (text) => this.setState({ title: text });
  render() {
    return (
      <View style={styles.container}>
        <Text
          style={{
            fontSize: 30,
            textAlign: "center",
            marginTop: 20,
            marginBottom: 30,
          }}
        >
          Search Movie
        </Text>
        <TextInput
          style={{
            height: 40,
            borderWidth: 1,
            width: 350,
            alignSelf: "center",
            borderRadius: 20,
            paddingHorizontal: 20,
          }}
          value={this.state.title}
          placeholder="Input Movie Title....."
          onChangeText={this.onChangeTitle}
        />
        <TouchableOpacity
          style={{
            backgroundColor: "#C4C4C4",
            height: 40,
            marginTop: 20,
            marginBottom: 40,
          }}
          onPress={() => this.fetchData()}
        >
          <Text
            style={{
              fontWeight: "bold",
              textAlign: "center",
              marginTop: 10,
              marginHorizontal: 20,
            }}
          >
            Search
          </Text>
        </TouchableOpacity>
        <FlatList
          data={this.state.data}
          keyExtractor={(x, i) => i}
          renderItem={({ item }) => (
            <TouchableOpacity>
              <View
                style={{
                  backgroundColor: "#C4C4C4",
                }}
              >
                <View
                  style={{
                    backgroundColor: "white",
                    marginVertical: 30,
                    flexDirection: "row",
                    alignItems: "space-between",
                    justifyContent: "center",
                  }}
                >
                  <Text style={styles.listTitle}>{item.Title}</Text>
                  <Text style={styles.listYear}>({item.Year})</Text>
                </View>
                <Image
                  style={{ height: 150, width: 120, alignSelf: "center" }}
                  source={{
                    uri: item.Poster,
                  }}
                />
              </View>
            </TouchableOpacity>
          )}
        ></FlatList>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listTitle: {
    fontWeight: "bold",
    fontSize: 18,
    paddingVertical: 20,
  },
  listYear: {
    fontSize: 14,
    fontWeight: "bold",
    paddingVertical: 20,
  },
});
