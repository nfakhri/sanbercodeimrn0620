import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Platform,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={{ fontSize: 30, textAlign: "center" }}>About Me</Text>

        <View style={styles.content}>
          <Image
            source={require("../assets/images/profile.jpg")}
            style={{ borderRadius: 25, alignSelf: "center" }}
          />
          <Text
            style={{
              marginTop: 10,
              marginBottom: 20,
              fontSize: 28,
              textAlign: "center",
            }}
          >
            Nurrul Fakhri
          </Text>
        </View>
        <View style={styles.description}>
          <Text
            style={{
              marginTop: 5,
              fontSize: 18,
              textAlign: "center",
              fontWeight: "bold",
            }}
          >
            Description
          </Text>
          <Text style={styles.description}>
            This application is made to fulfill the final project task of
            SanberCode react Native BootCamp
          </Text>
        </View>

        <Text
          style={{
            marginTop: 10,
            fontSize: 18,
            textAlign: "center",
            fontWeight: "bold",
          }}
        >
          Contact
        </Text>
        <View style={{ marginLeft: 115, flexDirection: "row", marginTop: 20 }}>
          <MaterialIcons name="email" size={24} color="black" />
          <Text style={{ fontSize: 14, marginLeft: 8 }}>
            nurrulfakhr28@gmail.com
          </Text>
        </View>
        <View style={{ marginLeft: 117, flexDirection: "row" }}>
          <Ionicons name="logo-instagram" size={25} color="black" />
          <Text style={{ fontSize: 14, marginLeft: 12 }}>@nfakhri12</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    padding: 20,
  },
  content: {
    marginTop: 20,
  },
  description: {
    fontSize: 14,
    textAlign: "justify",
  },
});
