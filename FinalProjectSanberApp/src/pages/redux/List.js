import React, { Component } from "react";
import { View, TouchableOpacity, Text, StyleSheet } from "react-native";

export default class List extends Component {
  renderItem = (text, i) => {
    const { onPressItem } = this.props;

    return (
      <TouchableOpacity style={styles.item} onPress={() => onPressItem(i)}>
        <Text style={{ textAlign: "center" }}>{text}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const { list } = this.props;

    return <View>{list.map(this.renderItem)}</View>;
  }
}

const styles = StyleSheet.create({
  item: {
    marginBottom: 5,
    backgroundColor: "#C4C4C4",
    padding: 15,
    fontSize: 24,
  },
});
