import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Platform,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
} from "react-native";

export default class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require("../assets/images/logo.png")}
          style={{
            marginLeft: 130,
            marginTop: 20,
            marginBottom: 20,
          }}
        />
        <Text style={{ fontSize: 30, textAlign: "center" }}>LOGIN</Text>
        <View style={styles.containerLogin}>
          <Text style={styles.inputTitle}>Username</Text>
          <TextInput style={styles.textInput} />
          <Text style={styles.inputTitle}>Password</Text>
          <TextInput secureTextEntry={true} style={styles.textInput} />

          <View
            style={{
              width: 150,
              borderRadius: 30,
              overflow: "hidden",
              marginTop: 15,
            }}
          >
            <Button
              color="black"
              title="Login"
              onPress={() => this.props.navigation.navigate("Home")}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Register")}
          >
            <Text style={styles.textCreate}>
              Dont have account? Create Here
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  containerLogin: {
    marginHorizontal: 40,
    height: 300,
    alignItems: "center",
    justifyContent: "center",
  },
  inputTitle: {
    fontSize: 18,
    marginVertical: 7,
  },
  textInput: {
    height: 40,
    width: 300,
    borderRadius: 30,
    marginBottom: 10,
    borderWidth: 1,
    paddingHorizontal: 10,
  },
  textCreate: {
    marginTop: 20,
  },
});
