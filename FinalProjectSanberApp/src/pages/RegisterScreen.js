import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Platform,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
} from "react-native";

export default class RegisterScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require("../assets/images/logo.png")}
          style={{
            marginLeft: 130,
            marginTop: 50,
            marginBottom: 60,
          }}
        />
        <Text style={{ fontSize: 30, textAlign: "center" }}>
          Create Account
        </Text>
        <View style={styles.containerLogin}>
          <Text style={styles.inputTitle}>Username</Text>
          <TextInput style={styles.textInput} />
          <Text style={styles.inputTitle}>Email</Text>
          <TextInput style={styles.textInput} />
          <Text style={styles.inputTitle}>Password</Text>
          <TextInput style={styles.textInput} />
          <Text style={styles.inputTitle}>Repeat Password</Text>
          <TextInput style={styles.textInput} />
          <View
            style={{
              width: 150,
              borderRadius: 30,
              overflow: "hidden",
              marginTop: 15,
            }}
          >
            <Button
              color="black"
              title="Register"
              onPress={() => this.props.navigation.navigate("Login")}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Login")}
          >
            <Text style={styles.textCreate}>Already have account? Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  containerLogin: {
    // marginHorizontal: 20,
    height: 450,
    alignItems: "center",
    justifyContent: "center",
  },
  inputTitle: {
    fontSize: 18,
    marginVertical: 7,
  },
  textInput: {
    height: 30,
    width: 300,
    borderRadius: 30,
    marginBottom: 10,
    borderWidth: 1,
  },
  textCreate: {
    marginTop: 20,
  },
});
